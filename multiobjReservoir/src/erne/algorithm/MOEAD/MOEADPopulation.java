package erne.algorithm.MOEAD;

import erne.AbstractFitnessResult;
import erne.Individual;
import erne.MultiobjectiveAbstractFitnessResult;
import erne.Population;
import reactionnetwork.ReactionNetwork;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class MOEADPopulation extends Population {
	public MOEADPopulation(int size, ReactionNetwork initNetwork) {
		super(size, initNetwork);

	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// the number of the subproblems
	final int N = 28;
	// the number of the weight vectors in the neighborhood of each weight vector
	final int T = 10;
	// number of objectives
	final int m = 3;

	// ************
	
	
	// a uniform spread of N weight vectors
	double[][] lambda;
	// F-value of x^i
	AbstractFitnessResult[] FV;
	// the best value found so far for each objectives
	double[] z = new double[m];
	// store non-dominated solutions found during the search
	ArrayList<Individual> EP;
	// the T closest weight vectors to lambda^i
	ArrayList<int[]> B = new ArrayList<int[]>();
	private Individual[] prev_pop;
		
	
	// STEP 1
	void init(Individual[] pop) {
		// STEP 1.1
		EP = new ArrayList<Individual>();
		
		// STEP 1.2
		init_lambda();		
		for(int i=0; i<N; i++) {
			double[] distance = new double[N];
			for(int j=0; j<N; j++) {
				distance[j] = i != j ? cal_euclidean_distance(lambda[i], lambda[j]) : Double.POSITIVE_INFINITY;
			}
			B.add(get_best_index(distance));
		}
				
		// STEP 1.3 
		FV = new AbstractFitnessResult[N];
		for(int i=0; i < N; i++) {
			FV[i] = pop[i].getFitnessResult();
		}
		
		// STEP 1.4
		for(int i=0; i<m; i++) {
			z[i] = cal_best_fitness(i, FV);
		}
	}
	
	
	
	// STEP 2
	Individual[] update(Individual[] pop) {
		for(int i=0; i<N; i++) {
			
			// STEP 2.1
			int k = B.get(i)[(int)(Math.random() * T)];
			int l = B.get(i)[(int)(Math.random() * T)];
			Individual new_ind_k = pop[k];
			Individual new_ind_l = pop[l];
			
			// STEP 2.2
			Individual new_ind = compare_te(new_ind_k, new_ind_l, lambda[i]) ? new_ind_k : new_ind_l;;
			for(int j=0; j<3; j++) {
				k = B.get(i)[(int)(Math.random() * T)];
				Individual new_ind_tmp = pop[k];
				new_ind = compare_te(new_ind, new_ind_tmp, lambda[i]) ? new_ind : new_ind_tmp;
				
				new_ind_tmp = mutator.mutate(new_ind.clone());
				new_ind_tmp.parentIds.add(new_ind.getId());
				new_ind = compare_te(new_ind, new_ind_tmp, lambda[i]) ? new_ind : new_ind_tmp;
			}

			
			// STEP 2.3
			update_z(new_ind);
			
			// STEP 2.4
			for(int j : B.get(i)) {
				if(compare_te(pop[j], new_ind, lambda[j]))
					pop[j] = new_ind;
			}
			
			// STEP 2.5
			for(int j=0; j<EP.size(); j++) {
				if(isDominate(new_ind, EP.get(j))) {
					EP.remove(j);
				}
			}
			boolean is_dominate = false;
			for(int j=0; j<EP.size(); j++) {
				if(isDominate(EP.get(j), new_ind))
					is_dominate = true;
			}
			if(!is_dominate)
				EP.add(new_ind);
		}
		return pop;
	}

	  private int[] get_best_index(double[] distance) {
		    int[] ret = new int[T];
		    
		    for(int i=0; i<T; i++) {
		      double min = Double.POSITIVE_INFINITY;
		      int min_index = -1;
		      
		      for (int j = 0; j < distance.length; j++) {
		          double value = distance[j];
		          if (value < min) {
		              min = value;
		              min_index = j;
		          }
		      }
		      
		      ret[i] = min_index;
		      distance[min_index] = Double.POSITIVE_INFINITY;
		    }
		    return ret;
		  }
	
	private double cal_best_fitness(final int index, final AbstractFitnessResult[] FV) {
		double max = Double.NEGATIVE_INFINITY;
		for (int i = 0; i<FV.length; i++) {
			if(max < MultiobjectiveAbstractFitnessResult.getIthFitness(index, FV[i]))
				max = MultiobjectiveAbstractFitnessResult.getIthFitness(index, FV[i]);
		}
			
		return max;
	}
	
	private double cal_euclidean_distance(double[] x, double[] y) {
		double sum = 0;
		for(int i=0; i<x.length; i++) {
			sum += (x[i] - y[i]) * (x[i] - y[i]);
		}
		return Math.sqrt(sum);
	}
	
	private boolean compare_te(Individual ind, Individual new_ind, double[] lambda) {
		double g_te_old = cal_te(ind, lambda);
		double g_te_new = cal_te(new_ind, lambda);
		
		if(g_te_new < g_te_old)
			return true;
		else 
			return false;
	}
	
	private void init_lambda() {
	    lambda = new double[28][3];
	    int index = 0;
	    for(int i=0; i<7; i++) {
	      for(int j=0; j<(7-i); j++) {
	        double[] tmp = new double[3];
	        tmp[0] = 1.0d / 6 * i;
	        tmp[1] = (1.0d - tmp[0]) / (6-i) * j > 0 ? (1.0d - tmp[0]) / (6-i) * j : 0;
	        tmp[2] = 1.0d - tmp[0] - tmp[1] > 0 ? 1.0d - tmp[0] - tmp[1] : 0;  
	        lambda[index] = tmp;
	        index++;
	      }
	    }
	}
	
	private void update_z(Individual ind) {
		for(int i=0; i<m; i++) {
			if(z[i] < MultiobjectiveAbstractFitnessResult.getIthFitness(i, ind.getFitnessResult()))
				z[i] = MultiobjectiveAbstractFitnessResult.getIthFitness(i, ind.getFitnessResult());
		}
	}
	private boolean isDominate(Individual ind, Individual dominated) {
		for(int i=0; i<m; i++) {
			if(MultiobjectiveAbstractFitnessResult.getIthFitness(i, ind.getFitnessResult()) < MultiobjectiveAbstractFitnessResult.getIthFitness(i, dominated.getFitnessResult())) {
				return false;
			}		
		}
		return true;
	}
	
	private double cal_te(Individual ind, double[] lambda) {
		double max = Double.NEGATIVE_INFINITY;
		for(int i=0; i<m; i++) {
			double tmp = lambda[i] * Math.abs(MultiobjectiveAbstractFitnessResult.getIthFitness(i, ind.getFitnessResult()) - z[i]);
			if(max < tmp)
				max = tmp;
		}
		return max;
	}

	@Override
	public Individual[] reproduction() {
		System.out.println("Gen: " + (populations.size()-1));
		prev_pop = update(prev_pop);
		Individual[] ret = new Individual[N];
		for(int i=0; i<N; i++) {
			if(i < EP.size())
				ret[i] = EP.get(i);
			else
				ret[i] = prev_pop[i];
		}
		
		return ret;
	}
	
	@Override
	public Individual[] resetPopulation() throws InterruptedException, ExecutionException {
		System.out.println("MOEAD, Gen: " + (populations.size()-1));
		prev_pop = super.resetPopulation();
		init(prev_pop);
		
		return prev_pop;
	}
	@Override
	public void checkRestart() {}
	
}
