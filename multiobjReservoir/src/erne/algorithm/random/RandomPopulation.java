package erne.algorithm.random;

import java.util.concurrent.ExecutionException;

import erne.Individual;
import erne.Population;
import reactionnetwork.ReactionNetwork;

public class RandomPopulation extends Population {

	public RandomPopulation(int size, ReactionNetwork initNetwork) {
		super(size, initNetwork);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Individual[] reproduction() {
		System.out.println("Gen: " + (populations.size()-1));
		return next_gen(populations.get(populations.size() - 1));
	}

	private Individual[] next_gen(Individual[] individuals) {
		Individual ret[] = new Individual[individuals.length];
		for(int i=0; i< individuals.length; i++) {
			Individual ind_tmp = individuals[i].clone();
			int max = (int)(Math.random() * 50);
			for(int j=0; j<max; j++)
				ind_tmp = mutator.mutate(ind_tmp);
			ind_tmp.parentIds.add(individuals[i].getId());
			ret[i] = ind_tmp;
		}
		return ret;
	}

	@Override
	public void checkRestart() {}
	
	@Override
	public Individual[] resetPopulation() throws InterruptedException, ExecutionException {
		System.out.println("Random, Gen: " + (populations.size()-1));
	
		return super.resetPopulation();
	}
	

}
