package evo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import erne.AbstractFitnessResult;
import erne.FitnessDisplayer;
import erne.InterfaceReservoirFitnessResult;
import model.PlotFactory;

public class ReservoirFitnessDisplayer implements FitnessDisplayer {
	
	private static final long serialVersionUID = 1L;

	public ReservoirFitnessDisplayer() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public JPanel drawVisualization(AbstractFitnessResult fitness) {
		InterfaceReservoirFitnessResult fitnessResult = (InterfaceReservoirFitnessResult) fitness;
		JPanel res = new JPanel();
		if (fitnessResult.getFitness() != 0.0) {
			ArrayList<double[]> targets = fitnessResult.getTargetOutput();
			ArrayList<double[]> actuals = fitnessResult.getActualOutput();
			for(int i = 0; i< Math.min(targets.size(), actuals.size());i++) {
//			double[] xData = new double[fitnessResult.timeSeries.get("a").length];
				Map<String, double[]> timeSeries = new HashMap<String, double[]>();
				timeSeries.put("Target", targets.get(i));
				timeSeries.put("Fitted", actuals.get(i));
				double[] xData = new double[actuals.get(i).length];
				for (int k = 0; k < xData.length; k++) xData[k] = k;
				res.add(new PlotFactory().createTimeSeriesPanel(timeSeries, xData, false, "Time [min]", ""));
			}
		}
		return res;		
	}

}
