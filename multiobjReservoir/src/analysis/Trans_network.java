package analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import erne.Individual;
import erne.MultiobjectiveAbstractFitnessResult;
import erne.Population;
 
public class Trans_network {

	public static Object deserialize(String path){
		try(FileInputStream fis = new FileInputStream(path)){
			ObjectInputStream in = new ObjectInputStream(fis);
			Object object = in.readObject();
			in.close();
			fis.close();
			return object;

			} catch ( IOException e ){
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return null;
	}
	
	private static ArrayList<ArrayList<Double>> getNetworksize(Individual[] pop,int gen) {
		ArrayList<ArrayList<Double>> ret = new ArrayList<ArrayList<Double>>();
		
		for(Individual ind : pop) {
			MultiobjectiveAbstractFitnessResult fitness = (MultiobjectiveAbstractFitnessResult)ind.getFitnessResult();
			ArrayList<Double> tmp = new ArrayList<Double>();
			tmp.add(MultiobjectiveAbstractFitnessResult.getIthFitness(0, fitness));
			tmp.add(MultiobjectiveAbstractFitnessResult.getIthFitness(1, fitness));
			tmp.add((double)ind.getNetwork().getNEnabledConnections());
			tmp.add((double)fitness.getRank());
			tmp.add((double)gen);
			ret.add(tmp);
		}

		return ret;
	}

	public static void main(String[] args) {
		if (args.length < 1){
			System.err.println("Usage:"+" path");
			return;
		}

		File dir = new File(args[0]);
		File[] list = dir.listFiles();
		ArrayList<ArrayList<ArrayList<ArrayList<Double>>>> info = new ArrayList<ArrayList<ArrayList<ArrayList<Double>>>>();
		
		if(list != null) {
			for(int i=0; i<list.length; i++) {
                if(list[i].isDirectory()) {
                		Population pop = (Population) deserialize(list[i] + "/population");
                		ArrayList<Individual[]> pops = pop.getAllPopulations();
                		for(int j = 0; j < pops.size(); j++) {
                			Individual[] population = pops.get(j);
                			ArrayList<ArrayList<ArrayList<Double>>> tmp = new ArrayList<ArrayList<ArrayList<Double>>>();
                			tmp.add(getNetworksize(population, j));
                			info.add(tmp);
                		}
                }
            }
        } else {
            System.out.println("No file in " + args[0] + "!");
        }
		
		System.out.println(info.size());
		
		StringBuilder sb = new StringBuilder();
//		sb.append("fitness,size,task" + System.getProperty("line.separator"));
		sb.append("Gen,fitness1,fitness2,size,rank" + System.getProperty("line.separator"));
		for(int i = 0; i < info.size(); i++) {
			ArrayList<ArrayList<ArrayList<Double>>> gen = info.get(i);
			for(ArrayList<ArrayList<Double>> fit : gen) {
				for(ArrayList<Double> ind : fit) {
//					sb.append(ind.get(0) + "," + ind.get(2) + ",Task1"+ System.getProperty("line.separator"));
//					sb.append(ind.get(1) + "," + ind.get(2) + ",Task2"+ System.getProperty("line.separator"));
					sb.append(ind.get(4) + "," + ind.get(0) + "," + ind.get(1) + "," + ind.get(2) + "," + ind.get(3) + System.getProperty("line.separator"));
				}
			}
		}
		
		try{
		  File file = new File("/Users/emi/Documents/workspace/lab/analysis/result_nnNSGA2.csv");
		  FileWriter filewriter = new FileWriter(file);

		  filewriter.write(sb.toString());
		  filewriter.close();
		}catch(IOException e){
		  System.out.println(e);
		}
	}
	
	
}