package erne;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;

import input.Input;
import model.OligoSystemComplex;
import reactionnetwork.Node;
import reactionnetwork.ReactionNetwork;
import task.Task;
import util.MyNNLS;
import util.MyOLSMultipleLinearRegression;
import utils.RunningStatsAnalysis;

public abstract class AbstractMultiobjectiveFitnessFunction extends AbstractFitnessFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final int numberOfObjectives = 3;
	protected String configString;
	//protected String task;
	//protected boolean nonNegative = false;
	protected HashMap<String, Input> inputs;
	protected int MAX_EVAL = 50;

	@Override
	public AbstractFitnessResult evaluate(ReactionNetwork network) {

		JsonObject config = Json.createReader(new StringReader(configString)).readObject();
		
		// initialize the network
		for (Node node : network.nodes) {
			node.protectedSequence = false;
			node.reporter = false;
			if (node.type == Node.INHIBITING_SEQUENCE) {
				Node from ;
				Node to;
				if(node.name.contains("T")){
					String[] names = node.name.substring(1).split("T");
					from = network.getNodeByName(names[0]); // TODO: warning, very implementation dependent
					to = network.getNodeByName(names[1]);
				} else {
					from = network.getNodeByName(""+node.name.charAt(1)); // TODO: warning, very implementation dependent
					to = network.getNodeByName(""+node.name.charAt(2));
				}
				node.parameter = (double) 1.0 / 100.0 * Math.exp((Math.log(from.parameter) + Math.log(to.parameter)) / 2);
			}
		}


		// Simulate
		JsonArray tasks = config.getJsonArray("task");
		ArrayList<double[]> taskData = new ArrayList<double[]>();
		ArrayList<double[]> actualOutput  = new ArrayList<double[]>();
		ArrayList<RunningStatsAnalysis> rsaList  = new ArrayList<RunningStatsAnalysis>();	
		
		// debug 
//		String log = null;

		for(JsonValue eachTask : tasks) {
			RunningStatsAnalysis rsa = new RunningStatsAnalysis();
				
			for(int i = 0; i < MAX_EVAL; i++) {
				//  Make an OligoSystemComplex.
				OligoSystemComplex oligoSystem = new OligoSystemComplex(network);
				prepareOligo(oligoSystem,config);
				
				// Prepare task.
				Task task = Task.generateTask((JsonObject) eachTask, inputs);
				double[] taskArray = Arrays.copyOfRange(task.getData(), task.start, task.end);
					
				Map<String, double[]> timeSeries = oligoSystem.calculateTimeSeries();
				double[] output =  getActualOutput(task, timeSeries, config);
				double fitness = calculateFitness(output, taskArray);
				rsa.addData(fitness);
				if((rsa.getStandardError() < 0.2 && i > 5) || (i == MAX_EVAL -1)) {
					System.out.println("StandardError : " + rsa.getStandardError() + ", i: " + i);
					taskData.add(taskArray);
					actualOutput.add(output);
					break;
				}
			} 
			rsaList.add(rsa);	
		}
		AbstractFitnessResult ret = computeFitness(network, taskData, actualOutput, rsaList);
		return ret;
	}
	
	public abstract void prepareOligo(OligoSystemComplex oligoSystem, JsonObject config); /*Prepare the oligosystem (attach input, etc)*/
	
	public abstract AbstractFitnessResult computeFitness(ReactionNetwork network, ArrayList<double[]> targetOutput, ArrayList<double[]> actualOutput, ArrayList<RunningStatsAnalysis> rsaList);

	
	public abstract double calculateFitness(double[] output,double[] tasksdat);

	@Override
	public AbstractFitnessResult minFitness() {
		ArrayList<Double> a = new ArrayList<Double>();
		for(int i = 0; i < numberOfObjectives; i++){
			a.add(0.0d);
		}

		return new SimpleMultiobjectiveFitnessResult(a);
	}

	@Override
	public int getNumberOfObjects() {
		return numberOfObjectives;
	}
	
	private double[][] trimTimeSeries(Map<String, double[]> timeSeries, ArrayList<String> names, Task task) {
		double[][] result = new double[task.end - task.start][];
		for (int t = task.start; t < task.end; t++) {
			int j = t - task.start;
			result[j] = new double[timeSeries.size() + 1];
			result[j][0] = 1.0;
			for (int k = 0; k < names.size(); k++) result[j][k + 1] = timeSeries.get(names.get(k))[t];
		}
		return result;
	}
	
	public double[] getActualOutput(Task task, Map<String, double[]> timeSeries, JsonObject config) {
		ArrayList<String> names = new ArrayList<>(timeSeries.keySet());
		double[][] result = trimTimeSeries(timeSeries, names, task);				
		double[] taskData = Arrays.copyOfRange(task.getData(), task.start, task.end);
		
		MyOLSMultipleLinearRegression regression;
		if (config.containsKey("non_negative") && config.getBoolean("non_negative")) {
			regression = new MyNNLS();
		} else {
			regression = new MyOLSMultipleLinearRegression();
		}
		regression.setNoIntercept(true);
		regression.newSampleData(taskData, result);
		return regression.calculateEstimatedValues(result);
	}

}
