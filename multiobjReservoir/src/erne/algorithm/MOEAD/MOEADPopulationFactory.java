package erne.algorithm.MOEAD;

import erne.Population;
import erne.PopulationFactory;
import reactionnetwork.ReactionNetwork;

public class MOEADPopulationFactory extends PopulationFactory{
	@Override
	public Population createPopulation(int popSize, ReactionNetwork startingNetwork){
		return new MOEADPopulation(popSize, startingNetwork);
	}

}
