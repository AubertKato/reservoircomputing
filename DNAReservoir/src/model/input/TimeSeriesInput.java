package model.input;

import java.util.Arrays;

import model.chemicals.SequenceVertex;

public class TimeSeriesInput extends AbstractInput {
	
	private int startingtime = 0;
	private int privatetime=0;
	private Double[] inputValues;
	
	public TimeSeriesInput(Double[] inputVals) {
		this.inputValues = inputVals;
	}

	@Override
	public boolean hasSingularity() {
		
		return privatetime < inputValues.length;
	}

	@Override
	public double getNextSingularityTime() {
		
		return startingtime;
	}

	@Override
	public void consummeSingularity() {
		
		privatetime++;

	}

	@Override
	public double f(double t){
		double firstValue = ((t-this.startingtime)<inputValues.length && (t-this.startingtime>=0))?inputValues[(int) Math.floor(t)-this.startingtime]:0;
		double secondValue = ((t-this.startingtime)<inputValues.length-1 && (t-this.startingtime>=-1))?inputValues[((int)Math.floor(t))+1-this.startingtime]:firstValue;
		
		double rest =(Math.PI/2)*(t - Math.floor(t));
		rest = Math.sin(rest)*Math.sin(rest);
		
		return firstValue*(1-rest)+secondValue*rest;
		
		
	}

	@Override
	public String toMathematica(SequenceVertex seq) {
		
		return null;
	}

	@Override
	public void reset() {
		privatetime = startingtime;

	}

	public String toString(){
		return "TimeSeriesInput: "+Arrays.toString(inputValues);
	}

	@Override
	public void setTime(double valueOf) {
		this.privatetime = (int) valueOf;
		this.startingtime = (int) valueOf;
		
	}

}
