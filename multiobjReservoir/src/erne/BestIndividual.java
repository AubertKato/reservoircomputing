package erne;

public class BestIndividual {
	private double fitness;
	private int generation;
	private Individual individual;
	
	BestIndividual(int gen, Individual ind){
		this.fitness = ind.getFitnessResult().getFitness();
		this.generation = gen;
		this.individual = ind;
	}
	
	double getFitness() {
		return fitness;
	}
	
	int get_generation() {
		return generation;
	}
	
	Individual getIndivisual() {
		return individual;
	}
	
	void update(int gen, Individual ind) {
		this.fitness = ind.getFitnessResult().getFitness();
		this.generation = gen;
		this.individual = ind;
	}
}
