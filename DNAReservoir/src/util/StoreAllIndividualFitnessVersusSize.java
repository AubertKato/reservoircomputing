package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import erne.Evolver;
import erne.Individual;
import erne.Population;
import erne.PopulationInfo;
import erne.util.Serializer;
import evo.ReservoirFitnessDisplayer;
import evo.ReservoirFitnessFunction;
import reactionnetwork.Library;

public class StoreAllIndividualFitnessVersusSize {
	
	public static ArrayList<Individual[]> getAllIndivs(String resultDir) {
		Population population;
		
		try {
			population = (Population) Serializer.deserialize(resultDir + "/population");
			return population.getAllPopulations();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static Evolver getEvolver(String resultDir) {
		ReservoirFitnessFunction fitnessFunction = new ReservoirFitnessFunction("");
		Evolver evolver = null;
		try {
			evolver = new Evolver(Library.startingMath, fitnessFunction, new ReservoirFitnessDisplayer());
			evolver.setGUI(true);
			evolver.setReader(resultDir);
			
		}  catch (Exception e) {
			e.printStackTrace();
		}
		return evolver;
	}
	
	public static void main(String[] args) throws IOException {
		
		if(args.length <= 0) {
			System.err.println("Usage: <className> directoryFile");
			return;
		}
		
		String mainDirectory = args[0];
		
		
		File dir = new File(mainDirectory);
		
		double fitness = 0.0;
		Individual bestEver = null;
		
		
		if(!dir.exists() || !dir.canRead() || !dir.isDirectory()) {
			System.err.println("Folder does not exists or cannot be opened");
		}
		
		String outputFile = dir.getName()+".log";
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outputFile)));
		
		for(File subdir : dir.listFiles()) {
			ArrayList<Individual[]> allPop = getAllIndivs(subdir.getAbsolutePath());
			if(allPop != null) {
				for(int i=0; i<allPop.size(); i++) {
					for(int j=0; j<allPop.get(i).length; j++) {
						Individual indiv = allPop.get(i)[j];
						if(indiv.getFitnessResult().getFitness() > fitness) bestEver = indiv;
						bw.write(indiv.getFitnessResult().getFitness()+" "+indiv.getNetwork().nodes.size()+" "+indiv.getNetwork().getNEnabledConnections()+"\n");
					}
				}
			}
		}
		bw.write("###");
		bw.write(bestEver.toString());
		bw.close();
		
	}

}
