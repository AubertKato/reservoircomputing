package erne.algorithm.random;

import erne.Population;
import erne.PopulationFactory;
import reactionnetwork.ReactionNetwork;

public class RandomPopulationFactory extends PopulationFactory{
	@Override
	public Population createPopulation(int popSize, ReactionNetwork startingNetwork){
		return new RandomPopulation(popSize, startingNetwork);
	}

}
