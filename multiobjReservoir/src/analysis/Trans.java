package analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import erne.Individual;
import erne.MultiobjectiveAbstractFitnessResult;
import erne.Population;
 
public class Trans {

	public static Object deserialize(String path){
		try(FileInputStream fis = new FileInputStream(path)){
			ObjectInputStream in = new ObjectInputStream(fis);
			Object object = in.readObject();
			in.close();
			fis.close();
			return object;

			} catch ( IOException e ){
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return null;
	}
	
	private static double getBestIndividual(Individual[] pop) {
		double max = 0.0d;
		
		for(Individual ind : pop) {
			MultiobjectiveAbstractFitnessResult fitness = (MultiobjectiveAbstractFitnessResult)ind.getFitnessResult();
			double fit = fitness.getFitness();
			if(fit > max)
				max = fit;
		}
		return max;
	}

	public static void main(String[] args) {
		if (args.length < 1){
			System.err.println("Usage:"+" path");
			return;
		}

		File dir = new File(args[0]);
		File[] list = dir.listFiles();
		ArrayList<ArrayList<Double>> info = new ArrayList<ArrayList<Double>>();
		
		if(list != null) {
			for(int i=0; i<list.length; i++) {
                if(list[i].isDirectory()) {
                		Population pop = (Population) deserialize(list[i] + "/population");
                		ArrayList<Individual[]> pops = pop.getAllPopulations();
                		for(int j = 0; j < pops.size(); j++) {
                			Individual[] population = pops.get(j);
                			if(info.size() <= j) {
                				ArrayList<Double> tmp = new ArrayList<Double>();
                				tmp.add(getBestIndividual(population));
                				info.add(tmp);
                			}else {
                				ArrayList<Double> tmp = info.get(j);
                				tmp.add(getBestIndividual(population));
                				info.set(j, tmp);
                			}
                		}
                }
            }
        } else {
            System.out.println("No file in " + args[0] + "!");
        }
		
		System.out.println(info.size());
		
		StringBuilder sb = new StringBuilder();
		sb.append("fitness,Gen" + System.getProperty("line.separator"));
		for(int i = 0; i < info.size(); i++) {
			ArrayList<Double> gen = info.get(i);
			for(Double fit : gen) {
				sb.append(fit + "," + (i+1) + System.getProperty("line.separator"));
			}
		}
		
		try{
		  File file = new File("/Users/emi/Documents/workspace/lab/result.csv");
		  FileWriter filewriter = new FileWriter(file);

		  filewriter.write(sb.toString());
		  filewriter.close();
		}catch(IOException e){
		  System.out.println(e);
		}
		
	}
	
	
}