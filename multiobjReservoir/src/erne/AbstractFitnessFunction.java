package erne;

import java.io.Serializable;
import java.util.ArrayList;

import reactionnetwork.ReactionNetwork;

public abstract class AbstractFitnessFunction implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract AbstractFitnessResult evaluate(ReactionNetwork network);

	public abstract AbstractFitnessResult minFitness();
	
	public abstract int getNumberOfObjects();

	public AbstractFitnessResult computeFitness(ReactionNetwork network, ArrayList<double[]> targetOutput, ArrayList<double[]> actualOutput) {
		return null;
	}
}
