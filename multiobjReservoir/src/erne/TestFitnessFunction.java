package erne;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;

import javax.json.Json;
import javax.json.JsonObject;

import input.Input;
import model.OligoSystemComplex;
import model.chemicals.SequenceVertex;
import model.input.TimeSeriesInput;
import reactionnetwork.ReactionNetwork;
import utils.RunningStatsAnalysis;

public class TestFitnessFunction extends AbstractMultiobjectiveFitnessFunction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String inputNodeName = "a";

	public TestFitnessFunction(String configString) {
		//this.configString = configString;
		
		
		
		System.out.println("Config name: "+configString);
		BufferedReader bf = null;
		try {
		  bf = new BufferedReader(new FileReader(configString));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(bf!=null) {
			try {
				StringBuilder sb = new StringBuilder("");
				Iterator<String> it = bf.lines().iterator();
				while(it.hasNext()) {
					sb.append(it.next());
				}
				//JsonReader jsonReader = Json.createReader(new StringReader(sb.toString()));
				this.configString = sb.toString();
				bf.close();
				JsonObject config = Json.createReader(new StringReader(this.configString)).readObject();
				this.MAX_EVAL = config.getInt("max_eval", this.MAX_EVAL);
				MultiobjectiveReservoirFitnessResult.MAX = config.getInt("softmax_graphsize", MultiobjectiveReservoirFitnessResult.MAX);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		  
	}
	

	@Override
	public AbstractFitnessResult computeFitness(ReactionNetwork network, ArrayList<double[]> targetOutput, ArrayList<double[]> actualOutput, ArrayList<RunningStatsAnalysis> rsaList) {
		return new MultiobjectiveReservoirFitnessResult(network, targetOutput, actualOutput, rsaList);
	}

	@Override
	public void prepareOligo(OligoSystemComplex oligoSystem, JsonObject config) {
		
		this.inputs = Input.generateInputMap(config.getJsonArray("inputs"));
		for (Input input : inputs.values()) {
			attachInput(oligoSystem, input); // only one element actually.
		}
		
	}
	
	private void attachInput(OligoSystemComplex oligoSystem, Input input) {
		SequenceVertex vertex = oligoSystem.getEquiv().get(inputNodeName);
		
		vertex.inputs.clear();
		vertex.inputs.add(new TimeSeriesInput(input.getDataAsDouble()));
		
	}


	@Override
	public double calculateFitness(double[] output, double[] tasksdat) {
		return MultiobjectiveReservoirFitnessResult.calculateFitness(output, tasksdat);
	}

}
