package evo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import erne.Evolver;
import erne.mutation.MutationRule;
import erne.mutation.Mutator;
import erne.mutation.rules.AddActivation;
import erne.mutation.rules.AddInhibition;
import erne.mutation.rules.AddNode;
import erne.mutation.rules.DisableTemplate;
import erne.mutation.rules.MutateParameter;

public class RunMultiObjReservoir {

	
	public static int nsga_multiobjPopSize = 128;
	public static int moea_multiobjPopSize = 28;
	public static int random_multiobjPopSize = 30;
	public static int max_gen = 50;
	
	public static void main(String[] args) {
		
		Mutator test = new Mutator(new ArrayList<MutationRule>(Arrays.asList(new MutationRule[] {
				new DisableTemplate(1), new MutateParameter(90), new AddNode(2), new AddActivation(2), new AddInhibition(5) })));
		try {
			// NSGA2
			Evolver evo = new Evolver(nsga_multiobjPopSize, max_gen,
					reactionnetwork.Library.startingMath, new erne.TestFitnessFunction(args[0]), new evo.ReservoirFitnessDisplayer(), (new erne.algorithm.morikawaNSGAII.NSGAIIBuilder().mutator(test)).buildAlgorithm());
			// MOEAD
//			Evolver evo = new Evolver(moea_multiobjPopSize, Evolver.MAX_GENERATIONS,
//					reactionnetwork.Library.startingMath, new erne.TestFitnessFunction(args[0]), new evo.ReservoirFitnessDisplayer(), (new erne.algorithm.MOEAD.MOEADBuilder()).buildAlgorithm());

			// Random
			//Evolver evo = new Evolver(random_multiobjPopSize, Evolver.MAX_GENERATIONS,
			//		reactionnetwork.Library.startingMath, new erne.TestFitnessFunction(args[0]), new evo.ReservoirFitnessDisplayer(), (new erne.algorithm.random.RandomBuilder()).buildAlgorithm());
			evo.setGUI(true);
			if(args.length > 1) {
				evo.resumeEvolve(args[1]);
			} else {
			    evo.evolve();
			}
		} catch (IOException | ClassNotFoundException | InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
