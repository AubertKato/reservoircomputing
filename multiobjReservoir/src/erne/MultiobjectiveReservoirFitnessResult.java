package erne;

import java.util.ArrayList;
import java.util.List;

import reactionnetwork.ReactionNetwork;
import utils.RunningStatsAnalysis;

public class MultiobjectiveReservoirFitnessResult extends MultiobjectiveAbstractFitnessResult implements InterfaceReservoirFitnessResult{

	ArrayList<Double> fitnessValues = new ArrayList<Double>();

	public ArrayList<double[]> targetOutput;
	public ArrayList<double[]> actualOutput;
	public ReactionNetwork network;
	
	public static int MAX = 5;
	
	public MultiobjectiveReservoirFitnessResult(ReactionNetwork network, ArrayList<double[]> targetOutput, ArrayList<double[]> actualOutput, ArrayList<RunningStatsAnalysis> rsaList) {
		this.targetOutput = targetOutput;
		this.actualOutput = actualOutput;
		this.network = network;
		
		for(int i = 0; i < actualOutput.size(); i++) {
			fitnessValues.add(rsaList.get(i).getMean());
		}
		
		
		fitnessValues.add(calculateConnectionFitness(network.getNEnabledConnections()));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	static protected double calculateFitness(double[] actualOutput, double[] targetOutput) {
	    double rss = 0.0;
		double zmn = actualOutput[0], zmx = actualOutput[0];
	    for (int i = 0; i < actualOutput.length; i++) {
			double res = actualOutput[i] - targetOutput[i];
			rss += res * res;
			zmn = Math.min(zmn, actualOutput[i]);
			zmx = Math.max(zmx, actualOutput[i]);
		}
	    
	    return (double)Math.sqrt(actualOutput.length / rss);
	}
	
	protected double calculateConnectionFitness(int con) {
		if(con < MAX)
			return 1.0d;
		
		return 1.0d / (con - MAX + 1);
	}

	@Override
	public List<Double> getFullFitness() {
		// TODO Auto-generated method stub
		return fitnessValues;
	}

	@Override
	public ArrayList<double[]> getTargetOutput() {
		// TODO: In the future, it should change...
		return targetOutput;
	}

	@Override
	public ArrayList<double[]> getActualOutput() {
		// TODO: In the future, it should change...
		return actualOutput;
	}
	
	@Override
	public double getFitness() {
		double ret = 0.0d;
		for (double fitness : fitnessValues) {
			ret += fitness;
		}
		return ret;
	}
	

}
