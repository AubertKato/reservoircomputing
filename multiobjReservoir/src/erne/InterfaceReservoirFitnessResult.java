package erne;

import java.util.ArrayList;

public interface InterfaceReservoirFitnessResult {
	
	public ArrayList<double[]> getTargetOutput();
	public ArrayList<double[]> getActualOutput();
	public double getFitness();

}
