package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ParetoFromFullLog {

	public static final int NODE_SIZE = 0;
	public static final int TEMP_SIZE = 1;
	public static int SORT_PARAM = NODE_SIZE;
	
	public static HashMap<Integer,Double> getParetoFront(ArrayList<IndividualResult> indvs){
		HashMap<Integer,Double> paretoFront = new HashMap<Integer,Double>();
		
		for(int i = 0; i<indvs.size(); i++) {
			IndividualResult res = indvs.get(i);
			int index;
			switch(SORT_PARAM) {
			case NODE_SIZE:
				index = res.sizeNode;
				break;
			case TEMP_SIZE:
				index = res.sizeTemplate;
				break;
			default:
				index = 1; //We are simply looking for the best individual ever
			}
			
			//First, check if there is someone there
			if(paretoFront.containsKey(index)) {
				//are we better?
				if(paretoFront.get(index) >= res.fitness) {
					//No, so give up
					continue;
				}
			}
			//either we are better, or there's no one else.
			//Check if we are actually better than lower individuals
			//We maintain a pareto front over time, so we just have to find someone lower
			int index2 = -1;
			for(int j = index - 1; j>0; j--) {
				if(paretoFront.containsKey(j)) {
					index2 = j;
					break;
				}
			}
			if(index2 != -1 && paretoFront.get(index2) >= res.fitness) {
				continue; //give up on this one
			}
			
			//So far, so good, we are on the current front. Insert
			paretoFront.put(index, res.fitness);
			//No check the bigger values, and remove them if they aren't on the front anymore
			
			for(int index3 : new ArrayList<Integer>(paretoFront.keySet())) {
				if(index3 > index && paretoFront.get(index3)<= res.fitness) {
					paretoFront.remove(index3);
				}
			}
			
		}
		
		return paretoFront;
	}
	
	public static ArrayList<IndividualResult> openLogFile(String path) throws IOException{
		File file = new File(path);
		if (!file.exists() || !file.canRead()) {
			System.err.println("ERROR: File can't be open");
			return null;
		}
		
		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<IndividualResult> indvs = new ArrayList<IndividualResult>();
		String line;
		while(br.ready()) {
			line = br.readLine();
			String[] split = line.split("\\s");
			if(split[0].startsWith("#")) {
				//done
				br.close();
				return indvs;
			}
			if(split.length < 3) {
				System.err.println("ERROR: Wrong file format");
			}
			double fits = Double.valueOf(split[0]);
			int size1 = Integer.valueOf(split[1]);
			int size2 = Integer.valueOf(split[2]);
			indvs.add(new IndividualResult(fits,size1,size2));
		}
		br.close();
		return indvs;
	}
	
	public static void main(String[] args) {
		if(args.length < 1) {
			System.err.println("No log provided");
		} else {
			try {
				HashMap<Integer,Double> front = getParetoFront(openLogFile(args[0]));
				for(int index : front.keySet()) {
					System.out.println(index+" "+front.get(index));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}
	
	
	protected static class IndividualResult {
		
		double fitness;
		int sizeNode;
		int sizeTemplate;
		
		IndividualResult(double fit, int sg, int st){
			fitness = fit;
			sizeNode = sg;
			sizeTemplate = st;
		}
		
	}
}
