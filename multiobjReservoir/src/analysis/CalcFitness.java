package analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import cluster.Cluster;
import erne.AbstractFitnessResult;
import erne.AbstractMultiobjectiveFitnessFunction;
import erne.Individual;
import erne.MultiobjectiveAbstractFitnessResult;
import erne.Population;
import erne.PopulationInfo;
import erne.TestFitnessFunction;
import erne.util.Serializer;
import reactionnetwork.ReactionNetwork;
 
public class CalcFitness {
	
	
	public static HashMap<String, String> getOpts(String[] args){
		HashMap<String, String> opts = new HashMap<>();
		for (int i = 0; i < args.length; i++) {
			if (args[i].charAt(0) == '-') {
				opts.put(args[i], args[i + 1]);
				i++;
			}
		}
		return opts;
	}

	public static Object deserialize(String path){
		try(FileInputStream fis = new FileInputStream(path)){
			ObjectInputStream in = new ObjectInputStream(fis);
			Object object = in.readObject();
			in.close();
			fis.close();
			return object;

			} catch ( IOException e ){
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return null;
	}
	
	private static ArrayList<Double> getIndividualInfo(ReactionNetwork ind, AbstractMultiobjectiveFitnessFunction fitnessFunction) {
		ArrayList<Double> ret = new ArrayList<Double>();
		
		AbstractFitnessResult fitness = fitnessFunction.evaluate(ind);
		
		ret.add(MultiobjectiveAbstractFitnessResult.getIthFitness(0, fitness));
		ret.add(MultiobjectiveAbstractFitnessResult.getIthFitness(1, fitness));
		ret.add((double)ind.getNEnabledConnections());

		return ret;
	}
	
	public static void reevaluatePopulation(String folder,ReactionNetwork[] ret, AbstractMultiobjectiveFitnessFunction fitnessFunction) {
		
	    if (ret == null) {
	    	System.err.println("No reaction networks");
	    	return;
	    }
		
		StringBuilder sb = new StringBuilder();
		sb.append("Gen,fitness1,fitness2,size" + System.getProperty("line.separator"));
		for(int i=0; i < ret.length; i++) {
			ArrayList<Double> ind =  getIndividualInfo(ret[i], fitnessFunction);
			sb.append(i + "," + ind.get(0) + "," + ind.get(1) + "," + ind.get(2) + System.getProperty("line.separator"));
		}
		
		try{
			  String[] inputPath = folder.split(File.separator);
			  File file = new File(inputPath[inputPath.length-1]+"result_best.csv");
			  FileWriter filewriter = new FileWriter(file, true);

			  filewriter.write(sb.toString());
			  filewriter.close();
			}catch(IOException e){
			  System.out.println(e);
			}
		System.out.println("Done with "+folder);
	}

	public static ReactionNetwork[] getBestReactionNetworks(String folder) throws ClassNotFoundException, IOException {
		Population population = (Population) Serializer.deserialize(folder + "/population");
		ReactionNetwork[] ret = new ReactionNetwork[population.getTotalGeneration()];
		
		for(int i = 0; i<population.getTotalGeneration(); i++){
			PopulationInfo info = population.getPopulationInfo(i);
			ret[i] = info.getBestIndividual().getNetwork();
		}
		System.out.println("Prepared "+folder);
		return ret;
	}
	
	public static void main(String[] args)  {
		if (args.length < 2){
			System.err.println("Usage:"+"[-m] path config");
			return;
		}

		HashMap<String, String> opts = getOpts(args);
		
		ArrayList<String> folders = new ArrayList<String>();
		if(opts.containsKey("-m")) {
			//batch mode
			File file = new File(opts.get("-m"));
			String[] directories = file.list(new FilenameFilter() {
			  @Override
			  public boolean accept(File current, String name) {
			    return new File(current, name).isDirectory();
			  }
			});
			for(int i = 0; i<directories.length; i++) {
				directories[i] = file.getAbsolutePath()+File.separator+directories[i];
			}
			folders.addAll(Arrays.asList(directories));
		} else {
			folders.add(args[0]);
		}
		
		AbstractMultiobjectiveFitnessFunction fitnessFunction = new TestFitnessFunction(args[args.length-1]);
		model.Constants.numberOfPoints = erne.Constants.maxEvalTime;
		
		HashMap<String,ReactionNetwork[]> bestNetworks = new HashMap<String,ReactionNetwork[]>();
		//Sequential, to make sure we don't go over the memory limitation for large folders
		folders.stream().forEach(folder -> {
			try {
				bestNetworks.put(folder, getBestReactionNetworks(folder));
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
		});
		System.out.println("Ready");
		//Parallel
		folders.parallelStream().forEach(folder -> reevaluatePopulation(folder,bestNetworks.get(folder), fitnessFunction));
		
		System.out.println("Finish!");

		
		
	}
	
	
}