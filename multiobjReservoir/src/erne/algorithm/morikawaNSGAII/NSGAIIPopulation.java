package erne.algorithm.morikawaNSGAII;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import erne.Individual;
import erne.MultiobjectiveAbstractFitnessResult;
import erne.Population;
import erne.PopulationInfo;
import erne.speciation.Species;
import reactionnetwork.ReactionNetwork;

public class NSGAIIPopulation extends Population {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1966721383358670350L;

	protected boolean superElitist;
//	protected transient FastNonDominatedSorter sorter;
	
	protected int factor = 2; //which fraction of the population do we keep to seed next generation
	
	// TODO: It should get like "getFitnessFunction().getNumberOfObjects();"
	private int numberOfObjectives = 3; 
	private Individual[] prev_pop;

	public NSGAIIPopulation(int size, ReactionNetwork initNetwork, boolean superElitist){
		super(size,initNetwork);
		this.superElitist = superElitist;
//		sorter = new FastNonDominatedSorter(superElitist);
	}

	@Override
	public Individual[] resetPopulation() throws InterruptedException, ExecutionException {
		System.out.println("Gen: " + (populations.size()-1));
		Individual[] pop = super.resetPopulation();
		Individual[] nextGenChildren = selection(pop);
		prev_pop = new Individual[pop.length * factor];
		
		System.arraycopy(pop, 0, prev_pop, 0, pop.length);
		System.arraycopy(nextGenChildren, 0, prev_pop, pop.length, nextGenChildren.length);

///		sorter.fastNonDominatedSort(pop); //Setting correctly the ranks
		fast_non_dominated_sort(nextGenChildren);	//Setting correctly the ranks
		
		return nextGenChildren;
	}

	@Override
	public Individual[] reproduction() {		
		Individual[] nextGenParents = getNextParents();
		Individual[] nextGenChildren = selection(nextGenParents);
		prev_pop = new Individual[nextGenParents.length * factor];

		System.arraycopy(nextGenParents, 0, prev_pop, 0, nextGenParents.length);
		System.arraycopy(nextGenChildren, 0, prev_pop, nextGenParents.length, nextGenChildren.length);                                   // combine parent and offspring population.
		
		fast_non_dominated_sort(nextGenChildren);
		
		return nextGenChildren;
	}

	public PopulationInfo getPopulationInfo(int i) {
		Individual[] indivs = populations.get(i);
//		ArrayList<HashSet<Individual>> sortedBy = (new FastNonDominatedSorter(false)).fastNonDominatedSort(indivs);
//		Species[] species = new Species[sortedBy.get(0).size()];
//		int index = 0;
//		for(Individual ind : sortedBy.get(0)){
//			species[index] = new Species(ind);
//			species[index].individuals = new ArrayList<Individual>(Arrays.asList(new Individual[]{ind}));
//			index++;
//		}
		List<Integer> front = fast_non_dominated_sort(indivs).get(0);
		Species[] species = new Species[front.size()];
		for(int index = 0; index < front.size(); ++index) {
			Individual ind = indivs[front.get(index)];
			species[index] = new Species(ind);
			species[index].individuals = new ArrayList<Individual>(Arrays.asList(new Individual[]{ind}));
		}
		
		return new PopulationInfo(populations.get(i), species);
	}

	@Override
	public void checkRestart() {
		// No paramter to set, so void
		//We should check that the last gen is correctly ranked
//		sorter = new FastNonDominatedSorter(superElitist);
		ArrayList<Individual> indivs = new ArrayList<Individual>();
		for(int i = 0; i<populations.size();i++){
			//add everybody so far
			indivs.addAll(Arrays.asList(populations.get(i)));
		}
		Individual[] indivArray = new Individual[indivs.size()];
		indivArray = indivs.toArray(indivArray);
//		sorter.fastNonDominatedSort(indivArray);
		fast_non_dominated_sort(indivArray);
	}


	/**
	 * Based on previous generations, determine the solutions that should be propagated.
	 * WARNING: original individuals are returned. Should not be modified
	 * @return
	 */
	public Individual[] getNextParents(){
		List<Individual> next_p = new ArrayList<Individual>();                 // Individual = [1.1d, 3.8d, ...], population = [Individual1, Individual2, ...]
//		Individual[] currentPop = populations.get(populations.size()-1);
		Individual[] currentPop = prev_pop;
		List<List<Integer>> front = fast_non_dominated_sort(currentPop);           // front = (f_0, f_1, ...). f_0 = [index_number_of_population, ...]
		
		int i = 0;
		while(next_p.size() + front.get(i).size() <= currentPop.length / factor){             // until the parent population is filled.
			List<Individual> tmp = new ArrayList<Individual>();
			for(int index : front.get(i)){
				tmp.add(currentPop[index]);
			}

			crowding_distance_assignment(tmp);
			next_p.addAll(tmp);                                                      // include ith nondominated front in the parent pop.
			i++;                                                                     // check the next front for inclusion.
		}

		List<Individual> tmp = new ArrayList<Individual>();
		for(int index : front.get(i)){
			tmp.add(currentPop[index]);
		}

		crowding_distance_assignment(tmp);
		tmp = new_dominate_sort(tmp);
		next_p.addAll(tmp.subList(0, (currentPop.length / 2) - next_p.size()));

		return next_p.toArray(new Individual[next_p.size()]);

	}


	/*
	 * This function do fast_non_dominated_sort.
	 * It receive population and return is front = (f_0, f_1, ...). f_0 = [indexNumber_of_population, ...].
	 * In this function, also set rank of individual.(HashMap<Individual,Integer> rank)
	 */
	List<List<Integer>> fast_non_dominated_sort(final Individual[] Pop){
		List<Individual> P = Arrays.asList(Pop);
		List<List<Integer>> S = new ArrayList<List<Integer>>();
		List<List<Integer>> front = new ArrayList<List<Integer>>();

		front.add(new ArrayList<Integer>());
		int[] n = new int[P.size()];

		for(int p = 0; p < P.size(); p++){
			S.add(new ArrayList<Integer>());
			n[p] = 0;

			for(int q = 0; q < P.size(); q++){
				if (compareFitness(P.get(p), P.get(q)) && (p != q)){         // If p dominates q
					S.get(p).add(q);                                       // Add q to the set of solutions dominated by p.
				}else if(compareFitness(P.get(q), P.get(p)) && (p != q)){
					n[p]++;                                              // Increment the domination counter of p.
				}
			}

			if(n[p] == 0){                                           // if p belongs to the first front.
				MultiobjectiveAbstractFitnessResult fit = (MultiobjectiveAbstractFitnessResult) P.get(p).getFitnessResult();
				fit.setRank(0);
				P.get(p).setRank(0);
				front.get(0).add(p);
			}  
		}

		int i = 0;                                                 // Initialize the front counter.
		while(front.get(i).size() > 0) {
			List<Integer> Q = new ArrayList<Integer>();              // Used to store the members of the next front.
			for(int p : front.get(i)) {
				for(int q : S.get(p)) {
					n[q] = n[q] - 1;
					if(n[q] == 0){           // q belongs to the first front.
						P.get(q).setRank(i+1);
						MultiobjectiveAbstractFitnessResult fit = (MultiobjectiveAbstractFitnessResult) P.get(q).getFitnessResult();
						fit.setRank(i+1);
						Q.add(q);
					}
				}
			}
			i = i + 1;
			front.add(Q);
		}
		return front;
	}

	/*
	 * Sort by values of i-th optimize function.
	 */
	Individual[] sort_by_values(final List<Individual> front, final int i){
		Individual sorted_list[] = new Individual[front.size()];
		List<Double> value_list = new ArrayList<Double>();

		for(Individual ind : front) {
			value_list.add(MultiobjectiveAbstractFitnessResult.getIthFitness(i, ind.getFitnessResult()));
		}

		Collections.sort(value_list);

		for(Individual ind : front){
			int index = value_list.indexOf(MultiobjectiveAbstractFitnessResult.getIthFitness(i, ind.getFitnessResult()));
			for(;index < front.size(); index++){
				if(sorted_list[index] == null){
					sorted_list[index] = ind;
					break;
				}
			}
		}

		return sorted_list;
	}

	/*
	 * Calculate crowding distance.
	 * It is in HashMap<Individual,Double> distance.
	 */
	void crowding_distance_assignment(List<Individual> front){
		Individual[] sorted_list;

		for(Individual ind : front){                                                                 // initialize distance.
			ind.setDistance(0.0d);
		}

		for(int i = 0; i < this.numberOfObjectives; i++) {
			sorted_list = sort_by_values(front, i);       // sort using each objective value.
			assert sorted_list.length ==  front.size(): "crowding_distance_assignment() : Size of list is different";

			double f_min = MultiobjectiveAbstractFitnessResult.getIthFitness(i, sorted_list[0].getFitnessResult());
			double f_max = MultiobjectiveAbstractFitnessResult.getIthFitness(i, sorted_list[sorted_list.length - 1].getFitnessResult());

			sorted_list[0].setDistance(Double.POSITIVE_INFINITY);						// so that boundary points are always selected.
			sorted_list[sorted_list.length - 1].setDistance(Double.POSITIVE_INFINITY);	

			for(int j = 1; j < sorted_list.length - 1; j++) {         // for all other points.
				if(f_max - f_min != 0){
					double after_val = MultiobjectiveAbstractFitnessResult.getIthFitness(i, sorted_list[j + 1].getFitnessResult());
					double befour_val = MultiobjectiveAbstractFitnessResult.getIthFitness(i, sorted_list[j - 1].getFitnessResult());
					sorted_list[j].setDistance(sorted_list[j].getDistance() + (after_val - befour_val) / (f_max - f_min));
				}
			}
		}
	}

	/*
	 * Return index of minimum value in argument array.
	 */
	int max(final double[] val){
		int ret = 0;

		for(int i = 1; i < val.length; i++){
			if(val[ret] < val[i])
				ret = i;
		}
		return ret;
	}

	/*
	 * Sort by crowding_distance, desc.
	 */
	List<Individual> new_dominate_sort(final List<Individual> list){
		List<Individual> ret = new ArrayList<Individual>();
		double val[] = new double[list.size()];

		for(int i = 0; i < list.size(); i++)
			val[i] = list.get(i).getDistance();

		for(int i = 0; i < list.size(); i++){
			int index = max(val);
			val[index] = Double.NEGATIVE_INFINITY;
			ret.add(list.get(index));
		}

		return ret;
	}

	/*
	 * Execute crossover and mutation. Make offspring from pop.
	 * Now Don not do crossover, only mutation.
	 */
	Individual[] selection(Individual[] p){
		List<Individual> ret = new ArrayList<Individual>();
		List<Individual> pop = Arrays.asList(p);

		List<Individual> pop_tmp = new ArrayList<Individual>(pop);
		Individual parent;
		Random rnd = new Random();

		for(int i = 0; i < pop.size(); i++){
			int r = rnd.nextInt(pop.size()- i) + i;
			Individual tmp = pop_tmp.get(r);
			pop_tmp.set(r, pop_tmp.get(i));
			pop_tmp.set(i, tmp);
		}

		for(int i = 0; i < pop.size(); i+=2){
			parent = tournament(pop_tmp.get(i), pop_tmp.get(i+1));
			ret.add(parent);
			ret.add(mutator.mutate(parent.clone()));
			ret.get(ret.size() - 1).parentIds.add(parent.getId());

		}

		for(int i = ret.size(); i < pop.size(); i++) {
			Individual tmp = pop_tmp.get(i);
			ret.set(i, mutator.mutate(tmp.clone()));
			ret.get(i).parentIds.add(tmp.getId());
		}
		return ret.toArray(new Individual[ret.size()]);
	}

	Individual tournament(Individual a, Individual b) {
		Individual ret = null;
		Random rnd = new Random();
		int flag = 0;

		if(doesDominates(a, b))
			flag = 1;
		else if(doesDominates(b, a))
			flag =  0;
		else if(rnd.nextInt(10) < 5)
			flag =  1;

		if(flag == 1) {
			ret = a.clone();
			ret.parentIds.add(a.getId());
		}else {
			ret = b.clone();
			ret.parentIds.add(b.getId());
		}

		return ret;
	}


	/*
	 * It return true when o1 is bigger than o2.
	 * It use only fitnessValue.
	 */
	public boolean compareFitness(final Individual o1, final Individual o2) {
		for(int i = 0; i < this.numberOfObjectives; i++) {
			double d1 = MultiobjectiveAbstractFitnessResult.getIthFitness(i, o1.getFitnessResult());
			double d2 = MultiobjectiveAbstractFitnessResult.getIthFitness(i, o2.getFitnessResult());
			if (d1 < d2) 
				return false;
		}
		return true;
	}
	
	/*
	 * It return true when o1 dominates than o2.
	 * First, it use rank, after that it is compared by crowding_distance.
	 */
	public boolean doesDominates(Individual o1, Individual o2) {
		if((o1.getRank() < o2.getRank()))
			return true;
		else if((o1.getRank() ==  o2.getRank()) && (o1.getDistance() > o2.getDistance()))
			return true;
		return false;
	}

}
